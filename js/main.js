    /*
    $(document).ready(function () {
        var mySwiper = new Swiper ('.content-services', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,

            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',

            autoHeight: false,
            autoplay: '7000',
            parallax: true,    
        });
    });  
    */
$(document).ready(function () {
    /* SMOOTH SCROLL */
    $('a.scroll').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                   $('html,body').animate({
                       scrollTop: target.offset().top - 0
                   }, 600);
                   return false;
               }
           }
    });

    /* RESPONSIVE MENU*/
    var widthSM = $(window).width() < 1024;
    if (widthSM) {
        $('#menu-icon').click(function(){
            $('#nav-menu').slideToggle(400);
        });

        $('li a[href^="#"], li a[href^="."]').click( function(){
            var scroll_el = $(this).attr('href');
            if ($(scroll_el).length != 0) {
                $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500);
                $('#nav-menu').slideToggle(200);
            }
            return false;
        });
    }
 
     /*-----VIDEO-------*/
    $('.video').click(function (event) {
        $(this).addClass('hide-before');
    });
    
    /* FIX MENU */
    var widthLG = $(window).width() > 1024;
    if (widthLG) {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 0) {
              $('#header-menu').addClass("fixed");
            }
            else{
              $('#header-menu').removeClass("fixed");
            }
        });
    }
    else {
        $('#callback-btn').addClass("static");
        $('#menu-icon,#nav-menu,#logo,.bg-responsive').addClass("fixed");
    }
    /*
    $('#callback-btn').click(function(){
        $('.modal-window').show();
    });
    */
    

    /* VIDEO PORTFOLIO */
    $('.video-gallery').slick({
        autoplay : false,
        arrows : true,
        draggable : false,
        dots : false,
        asNavFor : '.video-gallery-sm',
        responsive : [{
            breakpoint : 768,
            settings : {
                dots : false,
                arrows : false,
                draggable : false
            }
        }]
    });
    
    $('.video-gallery-sm').slick({
        autoplay : false,
        arrows : false,
        draggable : false,
        dots : false,
        slidesToShow : 5,
        centerMode : true,
        asNavFor : '.video-gallery',
        focusOnSelect : true,
        responsive : [{
            breakpoint : 768,
            settings : {
                slidesToShow : 3,
                dots : false,
                arrows : false,
                draggable : true,
                centerMode : true
                
            }
       }]
    });


});

    


